module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Denkmalmit',
    link: [
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700'
      }
    ],
    script: [
      {
        src:
          'https://cdn.jsdelivr.net/npm/basicscroll@2.0.0/dist/basicScroll.min.js'
      },
      {
        defer: true,
        src: 'https://use.fontawesome.com/releases/v5.0.8/js/all.js'
      }
    ]
  },

  meta: {
    description:
      'Denkmalmit ist der Verein hinter Denkmal.org, dem Online-Eventkalender für Basels Nachtleben.',
    theme_color: '#1a1a1a',
    ogHost: 'https://www.denkmalmit.org',
    ogImage: { path: '/images/denkmal-logo.png' },
    twitterCard: 'summary_large_image',
    twitterSite: '@denkmal_basel',
    twitterCreator: '@stophecom'
  },

  modules: [
    '@nuxtjs/pwa',
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-570541-7'
      }
    ],
    '@nuxtjs/sentry'
  ],

  sentry: {
    public_key: process.env.SENTY_TOKEN || '3ed4d1fea97f4df4b5424aff9b05a2e9',
    project_id: process.env.SENTRY_PROJECT_ID || '1367711',
    environment: process.env.NODE_ENV
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#8ba9a3' },

  plugins: [
    {
      src: '~/plugins/vueSocialSharing.js'
    },
    {
      src: '~/plugins/vueScrollReveal',
      ssr: false
    }
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
